# Git training

You must clone this repository and make two commits in the username.txt file.  First commit should contains only your first name, second commit should add your lastname.

Since the master branch is protected, you must use your own branch for this exercice and do a pull request to merge to master. I will be the approver for your pull request.  Your branch should be named feature/YourFirstName_YourLastName.

**warning**
If your branch is literally named feature/YourFirstName_YourLastName, you fail immediatly :smiley:

Please do this on your spare time and not during the working hours.

